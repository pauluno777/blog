import { Injectable } from '@nestjs/common';
import { CreateUserDto } from './dto/create-user.dto';
import { PrismaService } from 'src/prisma/prisma.service';
import { User } from '@prisma/client';
import { Observable, from } from 'rxjs';

@Injectable()
export class UserService {
  constructor(private prisma: PrismaService) {}

  create(createUserDto: CreateUserDto): Observable<User> {
    return from(this.prisma.user.create({ data: createUserDto }));
  }

  findAll(): Observable<User[]> {
    return from(this.prisma.user.findMany());
  }

  findOne(id: number): Observable<any> {
    return from(this.prisma.user.findFirst({ where: { id: id } }));
  }

  update(id: number, userDto: CreateUserDto): Observable<any> {
    const user = userDto;
    delete user.email;
    return from(this.prisma.user.update({ where: { id: id }, data: user }));
  }

  remove(id: number): Observable<any> {
    return from(this.prisma.user.delete({ where: { id: id } }));
  }
}
